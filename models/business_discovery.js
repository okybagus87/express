const mongoose = require("mongoose");
const schema = mongoose.Schema;

const bdSchema = new schema({
  id: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  followers_count: {
    type: Number,
    required: true,
  },
  media_count: {
    type: Number,
    required: true,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
});

module.exports = business_discovery = mongoose.model(
  "business_discovery",
  bdSchema
);
