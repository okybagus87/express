const express = require("express");
const router = express.Router();
const userService = require("../services/user");

router.get("/", async function (req, res, next) {
  try {
    res.json(await userService.getAll(req.query.page));
  } catch (err) {
    res.status(500).json(`Error while getting user`, err.message);
    next(err);
  }
});

router.get("/:id", async function (req, res, next) {
  try {
    res.json(await userService.getOne(req.params.id));
  } catch (err) {
    console.error(`Error while get user`, err.message);
    next(err);
  }
});

router.post("/", async function (req, res, next) {
  try {
    res.json(await userService.create(req.body));
  } catch (err) {
    res.status(500).json((`Error while creating user`, err.message));
    next(err);
  }
});

router.put("/:id", async function (req, res, next) {
  try {
    res.json(await userService.update(req.params.id, req.body));
  } catch (err) {
    console.error(`Error while updating user`, err.message);
    next(err);
  }
});

module.exports = router;
