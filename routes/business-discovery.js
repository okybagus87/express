const express = require("express");
const router = express.Router();
const bdService = require("../services/business-discovery");

router.get("/", async function (req, res, next) {
  try {
    res.json(await bdService.getAccount(req.query));
  } catch (err) {
    res.status(500).json(`Error while getting account`, err.message);
    next(err);
  }
});

module.exports = router;
