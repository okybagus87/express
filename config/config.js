const date_time = new Date();
let date = ("0" + date_time.getDate()).slice(-2);
let month = ("0" + (date_time.getMonth() + 1)).slice(-2);
let year = date_time.getFullYear();
let hours = date_time.getHours();
let minutes = date_time.getMinutes();
let seconds = date_time.getSeconds();
let now =
  year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

const config = {
  db: {
    host: "localhost",
    user: "root",
    password: "sayacakep87",
    database: "amanahummah",
    port: 3306,
  },
  date_now: now,
  listPerPage: 10,
};
module.exports = config;
