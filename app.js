const express = require("express");
const app = express();
const port = 3000;
const { default: mongoose } = require("mongoose");
const db = require("./config/mongo").mongoURI;
require("dotenv").config();

mongoose.connect(db).catch((err) => {
  console.log(err);
});

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);

app.get("/", (req, res) => {
  res.json({
    message: "Success",
  });
});

const userRoute = require("./routes/user");
app.use("/api/user", userRoute);

const bdRoute = require("./routes/business-discovery");
app.use("/api/business-discovery", bdRoute);

app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({
    message: err.message,
  });
  return;
});

app.use("/", (req, res) => {
  res.status(404);
  res.send("Not Found");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
