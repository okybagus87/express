const db = require("./db");
const helper = require("../config/helper");
const config = require("../config/config");
var bcrypt = require("bcrypt");

async function getAll(page) {
  const offset = helper.getOffset(page, config.listPerPage);

  const rows = await db.query(
    `SELECT * from users LIMIT ${offset},${config.listPerPage}`
  );

  const data = helper.emptyOrRows(rows);
  const meta = {
    page,
  };

  return {
    data,
    meta,
  };
}

async function getOne(id) {
  const rows = await db.query(`SELECT * from users where id=${id}`);

  const data = helper.emptyOrRows(rows);

  return data[0];
}

async function create(user) {
  let message = "Error in creating user";

  if (user.password && user.username && user.name) {
    const result = await db.query(
      `INSERT INTO users 
            (name, username, password, created_at, updated_at) 
            VALUES 
            ('${user.name}', '${user.username}', '${bcrypt.hashSync(
        user.password,
        10
      )}', '${config.date_now}', '${config.date_now}')`
    );

    if (result.affectedRows) {
      message = "User created successfully";
    }
  }

  return {
    message,
  };
}

async function update(id, user) {
  let message = "Error in updating user";
  var current = await getOne(id);

  if (user.name && user.username && user.new_password && user.old_password) {
    message = "Password invalid";
    var passValid = bcrypt.compareSync(user.old_password, current.password);

    if (passValid) {
      const result = await db.query(
        `UPDATE users SET 
                name='${user.name}', 
                username='${user.username}', 
                password='${bcrypt.hashSync(user.new_password, 10)}', 
                updated_at='${config.date_now}'
                WHERE id=${id}`
      );

      if (result.affectedRows) {
        message = "User updated successfully";
      }
    }
  }

  return {
    message,
  };
}

module.exports = {
  getAll,
  getOne,
  create,
  update,
};
