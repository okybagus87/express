const { default: axios } = require("axios");
const business_discovery = require("../models/business_discovery");

async function getAccount(req) {
  let data = "";
  await axios
    .get(
      "https://app.brandboss.id/api/social-listening/public-account/instagram/business-discovery/account",
      {
        params: req,
      }
    )
    .then((res) => {
      data = res.data.data.business_discovery;
    });

  const newData = new business_discovery({
    id: data.id,
    name: data.name,
    username: data.username,
    followers_count: data.followers_count,
    media_count: data.media_count,
  });

  return newData
    .save()
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
}

module.exports = {
  getAccount,
};
